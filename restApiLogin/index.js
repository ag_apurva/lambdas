var AWS = require("aws-sdk");
var docClient = new AWS.DynamoDB.DocumentClient();

var mysql = require('mysql');
var pool = mysql.createPool({
    // connectionLimit: 20,
    host: 'campuslive.czmocuynmaaq.us-east-2.rds.amazonaws.com',
    user: 'admin',
    password: 'Vishal#3',
    database: 'CampusLiveDev'
});

exports.handler = async(event) => {
    console.log('Received event:', JSON.stringify(event, null, 2));
    console.log('Query String email:', event.queryStringParameters.email);
    console.log('Query String code:', event.queryStringParameters.code);
    let email = event.queryStringParameters.email
    let code = event.queryStringParameters.code
    let sql4 = 'SELECT * FROM UserInfo INNER JOIN UniversityInfo where UserInfo.UserEmail=' + '"' + email + '"' + 'and UserInfo.UniCode=' + '"' + code + '"'+ 'and UserInfo.UniCode=UniversityInfo.Uni_Code';
    console.log(sql4);
    let codeAwait = await getOrder(sql4, "");
    console.log("codeAwait", codeAwait);
    var bodyData = {};
    if (codeAwait.length !== 0) {
        bodyData = codeAwait[0];
    }
    console.log("bodyData", JSON.stringify(bodyData))
    let statusCode = 200;
    if (Object.keys(bodyData) == 0) {
        statusCode = 401;
    }

    var res = {
        "statusCode": statusCode,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
        },
        "body": JSON.stringify(bodyData)
    };

    return res;

};


let getOrder = async(sql, params) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) {
                console.log("connection failed")
                reject(err);
            }
            else {
                connection.query(sql, params, (err, results) => {
                    if (err) {
                        console.log("query failed", err)
                        reject(err);
                    }
                    console.log("-----Query Done!");
                    connection.release();
                    //console.log("-----Data: ", results);
                    resolve(results);
                });
            }

        });
    });
};


/*
this is lambda behind the rest-api 
which is called to check if the university code and the email are correct,
if correct it returns the UsersData and its corresponding University data for the user.
*/