var AWS = require("aws-sdk");
var docClient = new AWS.DynamoDB.DocumentClient();
var aws = require("aws-sdk");
var ses = new aws.SES({ region: "us-east-2" });
var mysql = require('mysql');
var pool = mysql.createPool({
   // connectionLimit: 20,
   host: 'campuslive.czmocuynmaaq.us-east-2.rds.amazonaws.com',
   user: 'admin',
   password: 'Vishal#3',
   database: 'CampusLiveDev'
});


exports.handler = async(event) => {
   console.log("event");
   let res;
   if (event.resource === "/anotheruserrequest") {
      let email = event.queryStringParameters.email
      console.log("event1", JSON.stringify(event), email);
      let userEmail = email;
      let sql = 'SELECT UniversityInfo.Uni_Name,UserInfo.UniCode FROM UserInfo INNER JOIN UniversityInfo where UserInfo.UserEmail=' + '"' + userEmail + '"' + 'and UserInfo.UniCode=UniversityInfo.Uni_Code';
      var data = await getOrder(sql, 0);
      console.log("data", data)
      res = {
         "statusCode": 200,
         "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
            "Access-Control-Allow-Methods": "GET,OPTIONS",
            "Access-Control-Allow-Origin": "*",
         },
         "body": JSON.stringify(data)
      };
   }
   else {
      console.log("event2", JSON.stringify(event));
      console.log("formData", event.queryStringParameters.formData)
      let formData = JSON.parse(event.queryStringParameters.formData);
      console.log("formData", formData)
      console.log("data", data)
      try {

         let Uni_Name = formData.college_name //event.request.userAttributes['custom:college_name'] || "U_1";
         let Uni_Country = formData.country //event.request.userAttributes['custom:country'] || "UCountry_1";
         let Uni_PostalCode = formData.pincode //event.request.userAttributes['custom:pincode'] || "Uposta_l";

         let Uni_Logo = "Null";
         let UserEmail = formData.username //event.request.userAttributes.email;
         let UserName = formData.name //event.request.userAttributes.name;
         let UserPhone = formData.phone_number //event.request.userAttributes.phone_number;
         let UserActive = "True";
         let UserRole = "Admin";
         let IsTrial = 1



         // let Uni_Name = "U_2";
         // let Uni_Country = "UCountry_2";
         // let Uni_PostalCode = "Uposta_2";

         // let UserEmail = "2@gmail.com";
         // let UserName = "VG_2";
         // let UserPhone = "p2";



         let value = "count";
         var sql1 = `Select count(*) as ${value} from UniversityInfo`;
         let numberOfItems = await getOrder(sql1, 0);
         numberOfItems = parseInt(numberOfItems[0]['count']) + 1;
         console.log("numberOfItems", numberOfItems);
         let code = "TR"
         if (numberOfItems < 10) {
            code += "00" + numberOfItems
         }
         else if (numberOfItems > 10 && numberOfItems < 100) {
            code += "0" + numberOfItems
         }
         else {
            code += numberOfItems
         }
         console.log("code generated", code);
         let Uni_Code = code;
         let idUniversityInfo = ''
         var post_University = { idUniversityInfo, Uni_Code, Uni_Name, Uni_Country, Uni_PostalCode, Uni_Logo }
         var sql2 = 'INSERT INTO UniversityInfo SET ?'
         console.log("query", await getOrder(sql2, post_University));


         var sql3 = `Select count(*) as ${value} from UserInfo`;
         numberOfItems = await getOrder(sql3, 0);
         numberOfItems = parseInt(numberOfItems[0]['count']) + 1;
         console.log("numberOfItems", numberOfItems);
         code = "USR"
         if (numberOfItems < 10) {
            code += "00" + numberOfItems
         }
         else if (numberOfItems > 10 && numberOfItems < 100) {
            code += "0" + numberOfItems
         }
         else {
            code += numberOfItems
         }
         console.log("code generated", code);

         let idUserInfo = ''
         let UserCode = code;
         var date = new Date();
         date.setDate(date.getDate() + 14)
         let TrialEndDate = date.toString();
         let UniCode = Uni_Code;
         var post_User = { idUserInfo, UserCode, UserName, UserEmail, UserPhone, UserRole, UserActive, IsTrial, TrialEndDate, UniCode }
         var sql4 = 'INSERT INTO UserInfo SET ?'
         console.log("query", await getOrder(sql4, post_User));


         var university_details = {
            "name": Uni_Name,
            "code": Uni_Code
         }
         var paramsEmail = {
            Destination: {
               ToAddresses: [formData.username],
            },
            Message: {
               Body: {
                  Text: { Data: JSON.stringify(university_details) },
               },

               Subject: { Data: "Test Email" },
            },
            Source: "vishalg@innocurve.com",
         };

         await ses.sendEmail(paramsEmail).promise();
         res = {
            "statusCode": 200,
            "headers": {
               "Content-Type": "application/json",
               "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
               "Access-Control-Allow-Methods": "GET,OPTIONS",
               "Access-Control-Allow-Origin": "*",
            },
            "body": JSON.stringify(event)
         };


      }
      catch (e) {
         console.log("error post sign signup trigger", e);
         res = {
            "statusCode": 200,
            "headers": {
               "Content-Type": "application/json",
               "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
               "Access-Control-Allow-Methods": "GET,OPTIONS",
               "Access-Control-Allow-Origin": "*",
            },
            "body": JSON.stringify(e)
         };
      }

   }

   return res;
};


let getOrder = async(sql, params) => {
   return new Promise((resolve, reject) => {
      pool.getConnection((err, connection) => {
         if (err) {
            console.log("connection failed")
            reject(err);
         }
         else {
            connection.query(sql, params, (err, results) => {
               if (err) {
                  console.log("query failed", err)
                  reject(err);
               }
               console.log("-----Query Done!");
               connection.release();
               //console.log("-----Data: ", results);
               resolve(results);
            });
         }

      });
   });
};


/*
This Lambda sits behind the api gateway,it server 2 purpose 
when called with '/anotheruserrequest' 
it returnes university name and codes correspond to the user.

when called with '/anotherusersubmit'
it creates a new user in data base and sends the new codes on its registered email

*/